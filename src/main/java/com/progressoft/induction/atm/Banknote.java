package com.progressoft.induction.atm;

import java.math.BigDecimal;

public enum Banknote {

    FIVE_JOD(new BigDecimal("5.0")),
    TEN_JOD(new BigDecimal("10.0")),
    TWENTY_JOD(new BigDecimal("20.0")),
    FIFTY_JOD(new BigDecimal("50.0"));

    private final BigDecimal value;

    Banknote(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    public static Banknote getBanknoteCategory(int number) {
        if (number == 5)
            return Banknote.FIVE_JOD;
        if (number == 10)
            return Banknote.TEN_JOD;
        if (number == 20)
            return Banknote.TWENTY_JOD;
        if (number == 50)
            return Banknote.FIFTY_JOD;
        throw new IllegalArgumentException();
    }
}