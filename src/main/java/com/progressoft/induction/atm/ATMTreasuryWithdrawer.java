package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


/**
 * This class uses Dynamic programming to get the required banknotes for each withdrawal
 * Its algorithm is similar to the coin problem algorithm,
 * but it differs that it works at a limited number of banknotes for each banknote category,
 * and it constructs the solution rather than returning how many banknotes are required.
 */
public class ATMTreasuryWithdrawer {
    private final ATMTreasury treasury;
    private final static int[] banknotes = {50, 20, 10, 5};
    private int[][] banknotesFreqDP;
    private int[] requiredBanknotesDP;
    private int[] requiredNumberOfBanknotesDP;

    public ATMTreasuryWithdrawer(ATMTreasury treasury) {
        this.treasury = Objects.requireNonNull(treasury);
    }

    public List<Banknote> withdraw(BigDecimal amountOfMoney) {
        if (treasury.getAmountOfMoney().compareTo(amountOfMoney) < 0)
            throw new NotEnoughMoneyInATMException();
        setupArrays(amountOfMoney.intValue());
        List<Banknote> banknotes = calculateRequiredBanknotes(amountOfMoney.intValue());
        withdrawFromTreasury(banknotes);
        destroyArrays();
        return banknotes;
    }

    private void setupArrays(int number) {
        int[] banknotesFreqInTreasury = getFrequencyForBanknotesInTreasury();
        banknotesFreqDP = new int[number + 1][4];
        requiredBanknotesDP = new int[number + 1];
        requiredNumberOfBanknotesDP = new int[number + 1];
        for (int i = 0; i < banknotesFreqDP.length; i++)
            banknotesFreqDP[i] = Arrays.copyOf(banknotesFreqInTreasury, 4);
    }

    private int[] getFrequencyForBanknotesInTreasury() {
        int[] frequency = new int[4];
        frequency[0] = treasury.getNumberOfFiftyJODBanknotes();
        frequency[1] = treasury.getNumberOfTwentyJODBanknotes();
        frequency[2] = treasury.getNumberOfTenJODBanknotes();
        frequency[3] = treasury.getNumberOfFiveJODBanknotes();
        return frequency;
    }

    private List<Banknote> calculateRequiredBanknotes(int number) {
        for (int i = 1; i <= number; i++) {
            requiredNumberOfBanknotesDP[i] = 1_000_000;
            for (int k = 0; k < banknotes.length; k++)
                if (canMakeMove(i, k, banknotes[k]))
                    addBanknote(i, k);
        }
        return getBanknotes(number);
    }

    private boolean canMakeMove(int outerLoop, int innerLoop, int banknote) {
        return outerLoop - banknote >= 0
                && requiredNumberOfBanknotesDP[outerLoop - banknote] + 1 < requiredNumberOfBanknotesDP[outerLoop]
                && banknotesFreqDP[outerLoop - banknote][innerLoop] > 0;
    }

    private void addBanknote(int outerLoop, int innerLoop) {
        requiredNumberOfBanknotesDP[outerLoop] = requiredNumberOfBanknotesDP[outerLoop - banknotes[innerLoop]] + 1;
        requiredBanknotesDP[outerLoop] = banknotes[innerLoop];
        banknotesFreqDP[outerLoop] = Arrays.copyOf(banknotesFreqDP[outerLoop - banknotes[innerLoop]], 4);
        banknotesFreqDP[outerLoop][innerLoop]--;
    }

    private List<Banknote> getBanknotes(int number) {
        List<Banknote> coins = new ArrayList<>();
        while (number > 0) {
            if (requiredBanknotesDP[number] == 0)
                return new ArrayList<>();
            coins.add(Banknote.getBanknoteCategory(requiredBanknotesDP[number]));
            number -= requiredBanknotesDP[number];
        }
        return coins;
    }

    private void withdrawFromTreasury(List<Banknote> banknotes) {
        banknotes.forEach(banknote -> treasury.removeBanknotes(banknote, 1));
    }

    private void destroyArrays() {
        requiredBanknotesDP = requiredNumberOfBanknotesDP = null;
        banknotesFreqDP = null;
    }
}