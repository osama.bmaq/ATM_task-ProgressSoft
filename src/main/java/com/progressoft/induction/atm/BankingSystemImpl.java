package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class BankingSystemImpl implements BankingSystem {
    private final Map<String, BankAccount> accounts = new HashMap<>();

    @Override
    public BigDecimal getAccountBalance(String accountNumber) {
        return accounts.get(accountNumber).getAmountOfMoney();
    }

    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {
        accounts.get(accountNumber).debitMoney(amount);
    }

    public void addAccount(BankAccount account) {
        Objects.requireNonNull(account);
        accounts.put(account.getNumber(), account);
    }

    public BankAccount getAccount(String accountNumber) {
        return Optional.of(accounts.get(accountNumber)).orElseThrow(AccountNotFoundException::new);
    }

    public boolean isAccountExisted(String accountNumber) {
        return accounts.containsKey(accountNumber);
    }
}