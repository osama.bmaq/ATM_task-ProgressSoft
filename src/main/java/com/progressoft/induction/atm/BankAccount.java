package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.InsufficientFundsException;

import java.math.BigDecimal;

public class BankAccount {
    private final String number;
    private BigDecimal amountOfMoney = new BigDecimal(0L);

    public BankAccount(String number) {
        if (!isValidAccountNumber(number))
            throw new IllegalArgumentException();
        this.number = number;
    }

    public static boolean isValidAccountNumber(String number) {
        return number != null && number.matches("[0-9]{9}");
    }

    public String getNumber() {
        return number;
    }

    public BigDecimal getAmountOfMoney() {
        return amountOfMoney;
    }

    public void addMoney(BigDecimal amountOfMoney) {
        if (amountOfMoney.compareTo(BigDecimal.ZERO) <= 0)
            throw new IllegalArgumentException();
        this.amountOfMoney = this.amountOfMoney.add(amountOfMoney);
    }

    public void debitMoney(BigDecimal amountToDebit) {
        if (amountToDebit.compareTo(BigDecimal.ZERO) <= 0)
            throw new IllegalArgumentException();
        if (amountOfMoney.compareTo(amountToDebit) < 0)
            throw new InsufficientFundsException();
        this.amountOfMoney = amountOfMoney.subtract(amountToDebit);
    }
}