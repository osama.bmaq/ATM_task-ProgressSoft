package com.progressoft.induction.atm;

import java.math.BigDecimal;

import static com.progressoft.induction.atm.Banknote.*;

public class ATMTreasury {
    private BigDecimal amountOfMoney = new BigDecimal(0);

    private final BanknotesTreasury fiveJODTreasury = new BanknotesTreasury(FIVE_JOD);
    private final BanknotesTreasury tenJODTreasury = new BanknotesTreasury(TEN_JOD);
    private final BanknotesTreasury twentyJODTreasury = new BanknotesTreasury(TWENTY_JOD);
    private final BanknotesTreasury fiftyJODTreasury = new BanknotesTreasury(FIFTY_JOD);

    public BigDecimal getAmountOfMoney() {
        return amountOfMoney;
    }

    public int getNumberOfFiveJODBanknotes() {
        return fiveJODTreasury.getNumberOfBankNotes();
    }

    public int getNumberOfTenJODBanknotes() {
        return tenJODTreasury.getNumberOfBankNotes();
    }

    public int getNumberOfTwentyJODBanknotes() {
        return twentyJODTreasury.getNumberOfBankNotes();
    }

    public int getNumberOfFiftyJODBanknotes() {
        return fiftyJODTreasury.getNumberOfBankNotes();
    }

    public boolean isEmpty(){
        return amountOfMoney.equals(BigDecimal.ZERO);
    }

    public void addBanknotes(Banknote category, int numberToAdd) {
        if (category == FIVE_JOD)
            fiveJODTreasury.addBankNotes(numberToAdd);
        else if (category == TEN_JOD)
            tenJODTreasury.addBankNotes(numberToAdd);
        else if (category == TWENTY_JOD)
            twentyJODTreasury.addBankNotes(numberToAdd);
        else if (category == FIFTY_JOD)
            fiftyJODTreasury.addBankNotes(numberToAdd);
        updateAmountOfMoney();
    }

    public void removeBanknotes(Banknote category, int numberToRemove) {
        if (category == FIVE_JOD)
            removeBanknotesFromTreasury(fiveJODTreasury, numberToRemove);
        else if (category == TEN_JOD)
            removeBanknotesFromTreasury(tenJODTreasury, numberToRemove);
        else if (category == TWENTY_JOD)
            removeBanknotesFromTreasury(twentyJODTreasury, numberToRemove);
        else if (category == FIFTY_JOD)
            removeBanknotesFromTreasury(fiftyJODTreasury, numberToRemove);
        updateAmountOfMoney();
    }

    private void removeBanknotesFromTreasury(BanknotesTreasury treasury, int numberToRemove) {
        if (numberToRemove > treasury.getNumberOfBankNotes())
            throw new IllegalArgumentException();
        treasury.removeBanknotes(numberToRemove);
    }

    private void updateAmountOfMoney() {
        amountOfMoney = fiveJODTreasury.getAmountOfMoney()
                .add(tenJODTreasury.getAmountOfMoney())
                .add(twentyJODTreasury.getAmountOfMoney())
                .add(fiftyJODTreasury.getAmountOfMoney());
    }
}