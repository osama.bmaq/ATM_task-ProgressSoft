package com.progressoft.induction.atm;

import java.math.BigDecimal;
import java.util.Objects;

public class BanknotesTreasury {
    private final Banknote category;
    private BigDecimal amountOfMoney = new BigDecimal(0);
    private int numberOfBankNotes;

    public BanknotesTreasury(Banknote category) {
        this.category = Objects.requireNonNull(category);
    }

    public BigDecimal getAmountOfMoney() {
        return amountOfMoney;
    }

    public Banknote getBanknoteCategory() {
        return category;
    }

    public int getNumberOfBankNotes() {
        return numberOfBankNotes;
    }

    public void addBankNotes(int numberOfBankNotes) {
        if (numberOfBankNotes <= 0)
            throw new IllegalArgumentException();
        this.numberOfBankNotes += numberOfBankNotes;
        amountOfMoney = amountOfMoney.add(BigDecimal.valueOf(numberOfBankNotes * category.getValue().longValue()));
    }

    public void removeBanknotes(int numberOfBankNotes) {
        if (numberOfBankNotes <= 0 || numberOfBankNotes > this.numberOfBankNotes)
            throw new IllegalArgumentException();
        this.numberOfBankNotes -= numberOfBankNotes;
        amountOfMoney = amountOfMoney.subtract(BigDecimal.valueOf(numberOfBankNotes * category.getValue().longValue()));
    }
}
