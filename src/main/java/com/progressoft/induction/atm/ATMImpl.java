package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class ATMImpl implements ATM {
    private final BankingSystemImpl bankingSystem;
    private final ATMTreasuryWithdrawer withdrawer;

    public ATMImpl(ATMTreasury treasury, BankingSystemImpl bankingSystem) {
        withdrawer = new ATMTreasuryWithdrawer(Objects.requireNonNull(treasury));
        this.bankingSystem = Objects.requireNonNull(bankingSystem);
    }

    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {
        if (!bankingSystem.isAccountExisted(accountNumber))
            throw new AccountNotFoundException();
        if(bankingSystem.getAccount(accountNumber).getAmountOfMoney().compareTo(amount) < 0)
            throw new InsufficientFundsException();
        List<Banknote> withdrawnBanknotes = withdrawer.withdraw(amount);
        if (!withdrawnBanknotes.isEmpty())
            bankingSystem.debitAccount(accountNumber, amount);
        return withdrawnBanknotes;
    }
}