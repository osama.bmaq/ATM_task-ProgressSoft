package com.progressoft.induction.atm;


public class Main {
    public static void main(String[] args) {
        ATMTreasury treasury = ObjectsFactory.getATMTreasury();
        BankingSystemImpl bankingSystem = ObjectsFactory.getBankingSystem();
        ATMInteractiveInterface interactiveInterface = new ATMInteractiveInterface(treasury, bankingSystem);
        do {
            interactiveInterface.run();
            System.out.println();
        } while (!treasury.isEmpty());
    }
}