package com.progressoft.induction.atm;

import java.math.BigDecimal;
import java.util.*;

public class ATMInteractiveInterface {
    private final ATMTreasury treasury;
    private final BankingSystemImpl bankingSystem;
    private final ATM atm;

    private final Scanner in = new Scanner(System.in);

    public ATMInteractiveInterface(ATMTreasury treasury, BankingSystemImpl bankingSystem) {
        this.treasury = Objects.requireNonNull(treasury);
        this.bankingSystem = Objects.requireNonNull(bankingSystem);
        atm = new ATMImpl(treasury, bankingSystem);
    }

    public void run() {
        if (treasury.getAmountOfMoney().equals(BigDecimal.ZERO)) {
            System.out.println("We are sorry! There is no money in the ATM treasury!");
            return;
        }
        System.out.println("Hello Customer, ProgressSoft ATM here!");

        Optional<BankAccount> account = requestAccountNumber();
        if (!account.isPresent())
            return;

        showAmountOfMoneyInAccount(account.get());

        Optional<BigDecimal> amountToWithdraw = requestAmountOfMoneyToWithdraw(account.get());
        if (!amountToWithdraw.isPresent())
            return;

        List<Banknote> withdrawnBanknotes = withdrawMoneyFromAccount(account.get(), amountToWithdraw.get());
        if (withdrawnBanknotes.size() == 0) //Cannot withdraw for any reason(InsufficientFunds,NotEnoughMoneyInAtm,etc)
            return;

        showWithdrawnMoney(withdrawnBanknotes);
    }

    private Optional<BankAccount> requestAccountNumber() {
        System.out.print("Enter your account number : ");
        String number = in.next();
        if (!BankAccount.isValidAccountNumber(number)) {
            System.out.println("This is not a valid account number!");
            return Optional.empty();
        }
        if (!bankingSystem.isAccountExisted(number)) {
            System.out.println("This account is not existed!");
            return Optional.empty();
        }
        return Optional.of(bankingSystem.getAccount(number));
    }

    private void showAmountOfMoneyInAccount(BankAccount account) {
        System.out.println("Your account with number : " + account.getNumber() + " has " + getAmountOfJODs(account));
    }

    private String getAmountOfJODs(BankAccount account) {
        BigDecimal amount = bankingSystem.getAccountBalance(account.getNumber());
        if (amount.equals(BigDecimal.ONE) || amount.equals(BigDecimal.ZERO))
            return amount + " JOD";
        return amount + " JODs";
    }

    private Optional<BigDecimal> requestAmountOfMoneyToWithdraw(BankAccount account) {
        System.out.print("Enter amount of money to withdraw : ");

        String amountOfMoneyStr = in.next();
        if (!checkIfNumber(amountOfMoneyStr))
            return Optional.empty();

        BigDecimal amountOfMoney = new BigDecimal(amountOfMoneyStr);
        if (!checkIfValidAmountOfMoney(amountOfMoney))
            return Optional.empty();

        if (!checkMoneyAvailability(account, amountOfMoney))
            return Optional.empty();

        return Optional.of(amountOfMoney);
    }

    private boolean checkIfNumber(String amountOfMoneyStr) {
        if (!amountOfMoneyStr.matches("(-)?[0-9]+(\\.[0-9]+)?")) {
            System.out.println("This is not a valid number!");
            return false;
        }
        return true;
    }

    private boolean checkIfValidAmountOfMoney(BigDecimal amountOfMoney) {
        if (amountOfMoney.compareTo(BigDecimal.ZERO) <= 0) {
            System.out.println("Cannot withdraw negative or zero number of money!");
            return false;
        }
        if (!amountOfMoney.remainder(BigDecimal.valueOf(5L)).equals(BigDecimal.ZERO)) {
            System.out.println("Cannot withdraw number that is not multiple of 5!");
            return false;
        }
        return true;
    }

    private boolean checkMoneyAvailability(BankAccount account, BigDecimal amountOfMoney) {
        if (amountOfMoney.compareTo(account.getAmountOfMoney()) > 0) {
            System.out.println("There is no enough money in your account!");
            return false;
        }
        if (amountOfMoney.compareTo(treasury.getAmountOfMoney()) > 0) {
            System.out.println("There is no enough money in the ATM!");
            return false;
        }
        return true;
    }

    private List<Banknote> withdrawMoneyFromAccount(BankAccount account, BigDecimal amountOfMoney) {
        List<Banknote> withdrawnBankNotes = atm.withdraw(account.getNumber(), amountOfMoney);
        if (withdrawnBankNotes.size() == 0)
            System.out.println("Cannot combine " + amountOfMoney
                    + "JODs from the available banknotes categories in the ATM, We apologize!");
        return withdrawnBankNotes;
    }

    private void showWithdrawnMoney(List<Banknote> withdrawnBanknotes) {
        System.out.println("Here is your money : ");
        getNumberOfBanknotesForEachBanknoteCategory(withdrawnBanknotes)
                .forEach((k, v) -> System.out.println("\tx" + v + " " + k + " Banknote category"));
        System.out.println("We wish to serve you again soon, Thank you for choosing us!");
    }

    private Map<String, Integer> getNumberOfBanknotesForEachBanknoteCategory(List<Banknote> banknotes) {
        Map<String, Integer> numberOfBanknotesMap = new HashMap<>();
        banknotes.forEach(b -> {
            if (b == Banknote.FIVE_JOD)
                addToMap(numberOfBanknotesMap, "Five JOD");
            else if (b == Banknote.TEN_JOD)
                addToMap(numberOfBanknotesMap, "Ten JOD");
            else if (b == Banknote.TWENTY_JOD)
                addToMap(numberOfBanknotesMap, "Twenty JOD");
            else if (b == Banknote.FIFTY_JOD)
                addToMap(numberOfBanknotesMap, "Fifty JOD");
        });
        return numberOfBanknotesMap;
    }

    private void addToMap(Map<String, Integer> numberOfBanknotesMap, String banknoteName) {
        if (numberOfBanknotesMap.containsKey(banknoteName))
            numberOfBanknotesMap.put(banknoteName, numberOfBanknotesMap.get(banknoteName) + 1);
        else
            numberOfBanknotesMap.put(banknoteName, 1);
    }
}
