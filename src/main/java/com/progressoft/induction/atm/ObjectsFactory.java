package com.progressoft.induction.atm;

import java.math.BigDecimal;

import static com.progressoft.induction.atm.Banknote.*;
import static com.progressoft.induction.atm.Banknote.FIFTY_JOD;

public class ObjectsFactory {

    public static BankingSystemImpl getBankingSystem() {
        BankingSystemImpl bankingSystem = new BankingSystemImpl();
        bankingSystem.addAccount(createAccount("123456789", BigDecimal.valueOf(1000L)));
        bankingSystem.addAccount(createAccount("111111111", BigDecimal.valueOf(1000L)));
        bankingSystem.addAccount(createAccount("222222222", BigDecimal.valueOf(1000L)));
        bankingSystem.addAccount(createAccount("333333333", BigDecimal.valueOf(1000L)));
        bankingSystem.addAccount(createAccount("444444444", BigDecimal.valueOf(1000L)));
        return bankingSystem;
    }

    private static BankAccount createAccount(String number, BigDecimal amountOfMoney) {
        BankAccount account = new BankAccount(number);
        account.addMoney(amountOfMoney);
        return account;
    }

    public static ATMTreasury getATMTreasury() {
        ATMTreasury treasury = new ATMTreasury();
        treasury.addBanknotes(FIVE_JOD, 100);
        treasury.addBanknotes(TEN_JOD, 100);
        treasury.addBanknotes(TWENTY_JOD, 20);
        treasury.addBanknotes(FIFTY_JOD, 10);
        return treasury;
    }
}
